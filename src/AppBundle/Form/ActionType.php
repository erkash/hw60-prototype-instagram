<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActionType extends AbstractType
{
    const LABELS = ['authorization' => 'Авторизация', 'registration' => 'Регистрация', 'logout' => 'Logout'];
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add($options['label'], SubmitType::class, ['label'=> self::LABELS[$options['label']]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_action_type';
    }
}

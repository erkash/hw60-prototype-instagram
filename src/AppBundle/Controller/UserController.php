<?php

namespace AppBundle\Controller;

use AppBundle\Form\ActionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_basic_index');
        };

        $registration_form = $this->createForm(ActionType::class, null, [
            'label' => 'registration',
            'action' => $this->generateUrl('fos_user_registration_register')
        ])->createView();

        $authorization_form = $this->createForm(ActionType::class, null, [
            'label' => 'authorization',
            'action' => $this->generateUrl('fos_user_security_login')
        ])->createView();

        return $this->render('user/index.html.twig', [
            'registration_form' =>$registration_form,
            'authorization_form' =>$authorization_form
        ]);
    }
}

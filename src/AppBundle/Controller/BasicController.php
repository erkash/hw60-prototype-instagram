<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BasicController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        if(!$this->getUser()) {
            return $this->redirectToRoute('user_index');
        }
        return $this->render('AppBundle:Basic:index.html.twig', array(
            // ...
        ));
    }

}
